# Reddish Fox

A Lua colortheme based on [Niko Humalamäki](https://github.com/aparaatti/)'s
Vimscript ["Redish" Nvim colorscheme](https://github.com/aparaatti/redish.vim).

![screenshot](reddish-fox.png)

Some edits were made:

- some colors from the palette were tweaked;
- the background color is now always transparent. This means that
  the terminal background color is used;
- color columns are now visible. This lets you see which lines of the buffer
  exceed the column number recommended maximum;
- tested on modern Treesitter grammars.
