---Sets colors for one or more highlight groups.
---@param group string Highlight group name
---@param settings table Highlight settings
local function set_highlight_string(group, settings)
  vim.api.nvim_set_hl(0, group, settings)
end

---Sets colors for a list of highlight groups.
---@param groups table List of highlight groups names
---@param settings table Highlight settings
local function set_highlight_array(groups, settings)
  for _, group in pairs(groups) do
    set_highlight_string(group, settings)
  end
end

---Sets colors for one or more highlight groups.
---@param groups string|table (List of) highlight group(s) name(s)
---@param settings table Highlight settings
local function set_highlight(groups, settings)
  if type(groups) == 'string' then
    set_highlight_string(groups, settings)
  elseif type(groups) == 'table' then
    set_highlight_array(groups, settings)
  end
end

--- Theme load handler
local function on_load()
  if vim.g.colors_name then
    vim.cmd 'hi clear'
  end
  vim.g.colors_name = 'reddish-fox'
  vim.o.termguicolors = true
  for groups, settings in pairs(require('reddish-fox.groups').setup()) do
    set_highlight(groups, settings)
  end
end

return {
  config = {
    undercurl = true,
    underline = true,
    bold = true,
    italic = true,
    strikethrough = true,
    invert_selection = false,
    invert_signs = false,
    invert_tabline = false,
    invert_intend_guides = false,
    inverse = true,
    background = 'dark',
    contrast = 'hard',
    palette_overrides = {},
    overrides = {},
    dim_inactive = false,
    transparent_mode = true,
  },
  setup = function(self, config)
    self.config = vim.tbl_extend('force', self.config, config or {})
  end,
  load = on_load,
}
